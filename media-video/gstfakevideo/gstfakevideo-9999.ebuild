# Copyright 1999-2008 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: Exp $

inherit eutils subversion

ESVN_REPO_URI="http://gstfakevideo.googlecode.com/svn/trunk"
ESVN_PROJECT="gstfakevideo"

DESCRIPTION="Userspace video driver providing fake video source from gstreamer pipeline"
HOMEPAGE="http://code.google.com/p/gstfakevideo/"
LICENSE="GPL-2"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE=""

RDEPEND="media-libs/gstreamer"
DEPEND="${RDEPEND}"

src_unpack() {
	subversion_src_unpack
	sed -i \
		-e "s:^\(CFLAGS=\).*$:\1${CFLAGS} -Wall:" \
		-e "s:^\(CC=\).*$:\1$(tc-getCC):" \
		-e "s:^\(OPTIONS=\).*$:\1\-shared -fpic -Wl,-soname,\$\{\@\}:" \
		Makefile || die "sed failed"
}

src_compile() {
	emake || die "emake failed"
}

src_install() {
	into /usr/local
	dolib.so libgstfakevideo.so
	dobin gstfakevideo
	dodoc README README-AMD64 || die "dodoc failed"
}
