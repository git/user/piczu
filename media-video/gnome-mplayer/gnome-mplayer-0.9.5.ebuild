# Copyright 1999-2008 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

inherit gnome2

DESCRIPTION="MPlayer GUI for GNOME Desktop Environment"
HOMEPAGE="http://code.google.com/p/gnome-mplayer"
SRC_URI="http://${PN}.googlecode.com/files/${P}.tar.gz"

LICENSE="GPL-2"
SLOT="0"
KEYWORDS="~amd64 ~ppc ~x86"
IUSE="ipod libnotify alsa gnome nautilus musicbrainz"

RDEPEND=">=x11-libs/gtk+-2.12
	>=dev-libs/glib-2.14
	dev-libs/dbus-glib
	media-video/mplayer
	gnome? ( >=gnome-base/gconf-2 )
	nautilus? ( gnome-base/nautilus )
	ipod? ( media-libs/libgpod )
	libnotify? ( x11-libs/libnotify )
	musicbrainz? ( =media-libs/musicbrainz-3* )"
DEPEND="${RDEPEND}
	dev-util/pkgconfig
	sys-devel/gettext"

G2CONF="$(use_enable nautilus)
	$(use_enable gnome schemas-install)
	$(use_with gnome gconf-schema-file-dir=/etc/gconf/schemas)
	$(use_with gnome gconf)
	$(use_with gnome gio)
	$(use_with alsa)
	$(use_with libnotify)
	$(use_with ipod libgpod)
	$(use_with musicbrainz libmusicbrainz3)
	--disable-static"

DOCS="ChangeLog README DOCS/*.txt DOCS/tech/*.txt"

src_install() {
	gnome2_src_install
	# Install documentation by hand because it tries to install 0 byte size
	# files, with COPYING and INSTALL.
	rm -rf "${D}"/usr/share/doc/${PN}
}
