# Copyright 1999-2009 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI="2"

inherit qt4 multilib gnome2

MY_PV=${PV/_/-}
MY_P=${PN}-${MY_PV}

DESCRIPTION="Unofficial NAPI-PROJEKT clone for non-Windows systems"
HOMEPAGE="http://krzemin.iglu.cz/qnapi"
SRC_URI="mirror://sourceforge/${PN}/${MY_P}.tar.gz"
LICENSE="GPL-2"
SLOT="0"
KEYWORDS="~amd64 ~ppc ~x86"
IUSE="gnome kde kde4"

DEPEND="x11-libs/qt:4
	gnome? ( gnome-base/gconf:2
		gnome-extra/nautilus-actions )
	kde? ( 
		|| ( kde-base/konqueror:3.5
			kde-base/kdebase:3.5 )
	)
	kde4? ( kde-base/kdebase-meta:4 )"
RDEPEND="${DEPEND}"

S=${WORKDIR}/${MY_P}

DOCS="doc/ChangeLog doc/README*"

src_configure() {
	eqmake4 ${PN}.pro
}

src_install() {
	dobin ${PN}
	dodoc ${DOCS}
	doman doc/${PN}.1
	domenu doc/${PN}.desktop

	if use kde; then
		insinto /usr/share/apps/konqueror/servicemenus/
		doins doc/qnapi-download.desktop 
	fi

	if use kde4; then
		insinto /usr/$(get_libdir)/kde4/share/kde4/services/ServiceMenus/
		doins doc/qnapi-download.desktop 
	fi

	if use gnome; then
		insinto /etc/gconf/schemas
		doins doc/*.schemas
	fi

	insinto /usr/share/icons/
	doins res/qnapi*.png
}

pkg_postinst() {
	use gnome && gnome2_pkg_postinst
}

