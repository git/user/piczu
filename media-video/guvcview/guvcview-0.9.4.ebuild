# Copyright 1999-2008 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

inherit eutils

MY_P="${PN}-src-${PV}"
S="${WORKDIR}/${MY_P}"

DESCRIPTION="GTK+ UVC viewer"
HOMEPAGE="http://guvcview.berlios.de"
SRC_URI="http://download.berlios.de/guvcview/${MY_P}.tar.gz"

LICENSE="GPL"
SLOT="0"
KEYWORDS="~x86"
IUSE=""

DEPEND="dev-libs/glib 
	media-libs/libsdl 
	media-libs/libpng 
	media-libs/portaudio 
	media-sound/twolame"

RDEPEND="${DEPEND}"

src_install() {
	emake DESTDIR="${D}" install || die "Install failed"
	# Delete conflict docs
	rm -rf ${D}/usr/share/doc/${PN}
	dodoc ABOUT-NLS AUTHORS ChangeLog NEWS README TODO.tasks
}
