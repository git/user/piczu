# Copyright 2000-2006 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: /var/cvsroot/gentoo-x86/gnome-extra/slab/slab-1.9999.ebuild,v 1.11 2006/07/02 20:24:08 vapier Exp $

inherit eutils subversion mono gnome2

DESCRIPTION="The new Desktop Menu from SuSE Linux Enterprise by Novell"
HOMEPAGE="http://www.novell.com/products/desktop/preview.html"

# Have to set SRC_URI blank or gnome2 eclass tries to fetch ${P}.tar.gz
SRC_URI=""

ESVN_REPO_URI="http://svn.gnome.org/svn/gnome-main-menu/tags/GNOME_MAIN_MENU_0_9_10"

LICENSE="LGPL-2.1"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE="beagle nautilus doc tracker"

# We have USE flags depending on each other, which leads to this logic. We
# prefer an activated USE flag and override the dependent USE flags.

RDEPEND=">=net-misc/networkmanager-0.6.3
	>=sys-apps/dbus-0.30
	>=gnome-base/libgtop-2.14.1
	>=sys-apps/hal-0.5.7-r3
	beagle? ( app-misc/beagle )
	tracker? ( app-misc/tracker )
	nautilus? ( gnome-base/nautilus )"
DEPEND="${RDEPEND}
	doc? (
		app-doc/doxygen
	)
	gnome-base/gnome-common"

DOCS="AUTHORS COPYING ChangeLog README NEWS"

G2CONF="${G2CONF} $(use_enable nautilus nautilus-extension)"
G2CONF="${G2CONF} $(use_enable doc gtk-doc)"

src_unpack() {
	subversion_src_unpack
	cd ${S}

	gnome2_omf_fix
	epatch ${FILESDIR}/01-${PN}-autogen-noconfigure.patch
	use doc || epatch ${FILESDIR}/02-configure.in-remove-gtk-doc.patch
	if use tracker && ! use beagle ; then
		epatch ${FILESDIR}/03-tracker-search.patch
	elif ! use tracker && ! use beagle ; then
		epatch ${FILESDIR}/04-disable-search.patch
	fi

	./autogen.sh --libexecdir=/usr/libexec --sysconfdir=/etc --libdir=/usr/lib --includedir=/usr/include --sbindir=/sbin
}

pkg_postinst() {
	gnome2_pkg_postinst
	if use tracker && use beagle ; then
		ewarn "You have both beagle and tracker set in your USE flags."
		ewarn "Beagle will be used by default. If you would like to use"
		ewarn "tracker, emerge this package with the beagle USE flag unset."
	fi
}
