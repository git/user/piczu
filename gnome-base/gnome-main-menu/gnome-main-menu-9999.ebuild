# Copyright 2000-2006 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

inherit autotools eutils subversion gnome2

DESCRIPTION="The new Desktop Menu from SUSE"
HOMEPAGE="http://www.novell.com/products/desktop/preview.html"

# Have to set SRC_URI blank or gnome2 eclass tries to fetch package
SRC_URI=""

ESVN_REPO_URI="http://svn.gnome.org/svn/gnome-main-menu/trunk"

S="${WORKDIR}/${PN}"

LICENSE="LGPL-2.1"
SLOT="0"
KEYWORDS="~x86"
IUSE="beagle doc libssui nautilus tracker"

RDEPEND=">=dev-libs/glib-2.12
	>=x11-libs/gtk+-2.13.3
	>=gnome-base/libglade-2
	>=gnome-base/gnome-desktop-2
	>=gnome-base/gnome-panel-2
	>=gnome-base/librsvg-2
	>=gnome-base/gnome-menus-2
	>=gnome-base/gconf-2
	>=gnome-base/libgtop-2
	>=gnome-base/libgnome-2
	>=gnome-base/libgnomeui-2
	dev-libs/dbus-glib
	>=net-misc/networkmanager-0.7
	sys-apps/hal
	x11-libs/cairo
	x11-libs/pango
	libssui? ( >=gnome-extra/libssui-0.5.6 )
	nautilus? ( >=gnome-base/nautilus-2.6 )
	tracker? ( app-misc/tracker )"

DEPEND="${RDEPEND}
	doc? (
		dev-util/gtk-doc
	)"

src_unpack() {
	subversion_src_unpack
	cd ${S}

	gnome2_omf_fix

	epatch "${FILESDIR}"/gnome-main-menu-gentooifications.patch

	use libssui && epatch "${FILESDIR}"/gnome-main-menu-libssui.patch
	use doc || epatch "${FILESDIR}"/02-configure.in-remove-gtk-doc.patch
	if use tracker && ! use beagle ; then
		epatch ${FILESDIR}/03-tracker-search.patch
	elif ! use tracker && ! use beagle ; then
		epatch ${FILESDIR}/04-disable-search.patch
	fi

	G2CONF="`use_enable nautilus nautilus-extension`"
	intltoolize --force || die "intloolize failed"
	eautoreconf || die "eautoreconf failed"
}

pkg_postinst() {

	elog
	elog " If you want to have recent applications-support working, you should "
	elog " also use the patched gnome-panel and gnome-desktop packages from this "
	elog " overlay "
	elog
	if use tracker && use beagle ; then
		ewarn " You have both beagle and tracker set in your USE flags. "
		ewarn " Beagle will be used by default. If you would like to use "
		ewarn " tracker, emerge this package with the beagle USE flag unset. "
	fi
}
