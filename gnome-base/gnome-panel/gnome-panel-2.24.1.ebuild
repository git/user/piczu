# Copyright 1999-2008 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: Exp $

inherit autotools eutils gnome2

DESCRIPTION="The GNOME panel"
HOMEPAGE="http://www.gnome.org/"

LICENSE="GPL-2 FDL-1.1 LGPL-2"
SLOT="0"
KEYWORDS="~alpha ~amd64 ~hppa ~ia64 ~ppc64 ~sparc ~x86"
IUSE="doc eds networkmanager policykit slab"

RDEPEND=">=gnome-base/gnome-desktop-2.12
		 >=x11-libs/pango-1.15.4
		 >=dev-libs/glib-2.15.6
		 >=x11-libs/gtk+-2.13.1
		 >=dev-libs/libgweather-2.24.1
		 dev-libs/libxml2
		 >=gnome-base/libglade-2.5
		 >=gnome-base/libgnome-2.13
		 >=gnome-base/libgnomeui-2.5.4
		 >=gnome-base/libbonoboui-2.1.1
		 >=gnome-base/orbit-2.4
		 >=x11-libs/libwnck-2.19.5
		 >=gnome-base/gconf-2.6.1
		 >=gnome-base/gnome-menus-2.11.1
		 >=gnome-base/libbonobo-2
		 gnome-base/librsvg
		 >=dev-libs/dbus-glib-0.71
		 >=sys-apps/dbus-1.1.2
		 >=x11-libs/cairo-1.0.0
		 eds? ( >=gnome-extra/evolution-data-server-1.6 )
		 networkmanager? ( >=net-misc/networkmanager-0.6 )
		 policykit? (
			>=sys-auth/policykit-0.7
			>=gnome-extra/policykit-gnome-0.7 )"
DEPEND="${RDEPEND}
		app-text/scrollkeeper
		>=app-text/gnome-doc-utils-0.3.2
		>=dev-util/pkgconfig-0.9
		>=dev-util/intltool-0.40
		~app-text/docbook-xml-dtd-4.1.2
		doc? ( >=dev-util/gtk-doc-1 )"

DOCS="AUTHORS ChangeLog HACKING NEWS README"

pkg_setup() {
	G2CONF="${G2CONF}
			--disable-scrollkeeper
			--with-in-process-applets=clock,notification-area,wncklet
			$(use_enable policykit polkit)
			$(use_enable networkmanager network-manager)
			$(use_enable eds)"
}

src_unpack() {
	gnome2_src_unpack

	if use slab; then
		epatch ${FILESDIR}/${PN}-alt-f1-slab.patch
		epatch ${FILESDIR}/${PN}-recently-used-apps.patch
	fi
}

pkg_postinst() {
	local entries="/etc/gconf/schemas/panel-default-setup.entries"
	local gconftool="${ROOT}usr/bin/gconftool-2"

	if [ -e "$entries" ]; then
		einfo "setting panel gconf defaults..."

		GCONF_CONFIG_SOURCE=$("${gconftool}" --get-default-source)

		"${gconftool}" --direct --config-source \
			"${GCONF_CONFIG_SOURCE}" --load="${entries}"
	fi

	# Calling this late so it doesn't process the GConf schemas file we already
	# took care of.
	gnome2_pkg_postinst
}
