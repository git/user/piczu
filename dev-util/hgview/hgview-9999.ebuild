# Copyright 1999-2008 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: Exp $

inherit distutils mercurial

EHG_REPO_URI="http://www.logilab.org/cgi-bin/hgwebdir.cgi/hgview"

DESCRIPTION="Gtk and Qt Mercurial interfaces"
HOMEPAGE="http://www.logilab.org/project/name/hgview"
LICENSE="GPL-2"
SLOT="0"
KEYWORDS="~x86"
IUSE="gtk qt4"

RDEPEND="virtual/python
	gtk? ( dev-python/pygtk )
	qt4? ( dev-python/PyQt4 )"
DEPEND="${RDEPEND}"

S="${WORKDIR}/${PN}"

pkg_setup() {
	if ! use gtk && ! use qt4; then
		eerror "At least one of the following USE flags is required:"
		eerror "gtk or/and qt4"
		die "gtk or/and qt4 USE flag needed"
	fi
}
