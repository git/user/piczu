# Copyright 1999-2009 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI="2"

WANT_AUTOCONF="latest"
WANT_AUTOMAKE="latest"

inherit autotools eutils gnome2

DESCRIPTION="Me TV is a digital television (DVB) viewer for GNOME/GTK."
HOMEPAGE="http://me-tv.sourceforge.net"
SRC_URI="http://launchpadlibrarian.net/28961128/me-tv-0.9.6.tar.gz"

LICENSE="GPL-3"
SLOT="0"
KEYWORDS="~x86"
IUSE="+X +nls -doc -xine -mplayer -vlc +gstreamer -debug"

RDEPEND=">=dev-cpp/libgnomemm-2.20
	>=dev-cpp/libgnomeuimm-2.20
	>=dev-db/sqlite-3.3.9
	net-libs/gnet:2
	x11-libs/cairo[glitz]
	X? ( x11-libs/libXtst )
	xine? ( media-libs/xine-lib )
	mplayer? ( media-video/mplayer )
	vlc? ( media-video/vlc )
	gstreamer? ( media-libs/gstreamer )
	doc? ( dev-util/gtk-doc )"
DEPEND="${RDEPEND}
	dev-util/intltool"

G2CONF="$(use_enable debug) \
	$(use_enable nls) \
	$(use_enable xine xine-engine) \
	$(use_enable mplayer mplayer-engine) \
	$(use_enable vlc libvlc-engine) \
	$(use_enable gstreamer libgstreamer-engine) \
	$(use_enable doc gtk-doc) \
	$(use_with X x)"

DOCS="AUTHORS README ChangeLog"

pkg_setup() {
	if ! use xine && ! use mplayer && ! use vlc && \
		! use gstreamer; then
		eerror
		eerror "You nned to enable at least one engine."
		eerror "Please set one or more of the following USE flags:"
		eerror "xine mplayer vlc gstreamer"
		die "no engine"
	fi
}
