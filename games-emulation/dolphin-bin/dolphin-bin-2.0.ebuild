# Copyright 1999-2010 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI="2"

inherit eutils games

MY_PN="${PN/-bin}"
MY_P="${MY_PN}-${PV}"

DESCRIPTION="Gamecube and Wii emulator"
HOMEPAGE="http://dolphin-emu.com"
SRC_URI="x86? ( http://dolphin.jcf129.com/dolphin-2.0.i686.tar.bz2 )
	amd64? ( http://dolphin.jcf129.com/dolphin-2.0.amd64.tar.bz2 )"

LICENSE="GPL-2"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE="wiimote"

RDEPEND="=media-libs/openal-1*
	x11-libs/wxGTK:2.8
	media-gfx/nvidia-cg-toolkit
	media-libs/portaudio
	wiimote? ( games-util/wiiuse )"
DEPEND="${RDEPEND}"

if use x86; then
	S="${WORKDIR}/Binary/Linux-i686"
elif use amd64; then
	S="${WORKDIR}/Binary/Linux-x86_64"
fi

src_prepare() {
	# Remove SVN dirs
        rm -rf `find ${S} -type d -name .svn`
}

src_install() {
	local INSTDIR="${GAMES_PREFIX_OPT}/${MY_PN}"
	insinto ${INSTDIR}
	exeinto ${INSTDIR}
	doins -r lib plugins sys user
	doexe dsptool dolphin-emu
	games_make_wrapper dolphin-emu "./dolphin-emu" ${INSTDIR} "${INSTDIR}/lib ${INSTDIR}/sys ${INSTDIR}/plugins"
	make_desktop_entry dolphin-emu "Dolphin Emulator"
	prepgamesdirs
}
