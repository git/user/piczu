# Copyright 1999-2009 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $
EAPI=2

inherit eutils

MY_PN="${PN/-bin}"
MY_PV="${PR/r}"
MY_P="${MY_PN}-${MY_PV}"
DESCRIPTION="Gamecube and Wii emulator"
HOMEPAGE="http://dolphin-emu.com"
SRC_URI="http://www.cs.huji.ac.il/~elylevy/${MY_PN}/i686/${MY_P}-release.tar.bz2"

LICENSE="GPL-2"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE="+wiimote +portaudio"

DEPEND="${RDEPEND}"
RDEPEND="=media-libs/openal-1*
	x11-libs/wxGTK:2.8
	media-gfx/nvidia-cg-toolkit
	portaudio? ( media-libs/portaudio )
	wiimote? ( games-util/wiiuse )"
S="${WORKDIR}/Binary/Linux-i686"

src_install() {
	local INSTDIR="/opt/${MY_PN}"
	dodir ${INSTDIR}
	# Remove SVN dirs
	rm -rf `find ${S} -type d -name .svn`
	cp -pR ${S}/* ${D}${INSTDIR} || die
	# Which group?
	#fowners root:users ${INSTDIR}/Dolphin
	dosym ${INSTDIR}/Dolphin /usr/bin/Dolphin
}
