# Copyright 1999-2008 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI="2"

inherit fdo-mime eutils

DESCRIPTION="Tlen IM client"
HOMEPAGE="http://tlen.pl"
SRC_URI="
	amd64? ( http://j.o2.pl/idi/${PN}64-${PV}.bin )
	x86? ( http://j.o2.pl/idi/${P}.bin )"

LICENSE="tlen"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE="phonon"
PROPERTIES="interactive"
RESTRICT="strip"

RDEPEND="x11-libs/qt-gui:4
	phonon? ( x11-libs/qt-phonon:4 )"
DEPEND="${RDEPEND}
	x11-misc/xdg-utils"

S=${WORKDIR}

TLEN_DIR="/opt/tlen"
STATIC_FILES="./imageformats/* ./phonon_backend/* ./qt_libs/*"
SHARED_FILES="tlen_libs/libgg.so tlen-bin updater"

src_unpack() {
	unpack_makeself
	cd ${S}/data
	# Fix RPATH
	if use phonon; then
		# Change RPATH
		./chrpath -r ${TLEN_DIR}/tlen_libs ${SHARED_FILES} > /dev/null
		# Disable unset
		sed -i "s:unset:# unset:" tlen
		# Remove unneeded libs
		rm -rf qt_libs imageformats phonon_backend
	else
		# Change RPATH
		./chrpath -r ${TLEN_DIR}/qt_libs:${TLEN_DIR}/tlen_libs ${SHARED_FILES} > /dev/null
		./chrpath -r ${TLEN_DIR}/qt_libs ${STATIC_FILES} > /dev/null
	fi
	# Desktop icon fix
	sed -i "s:.jpg:.png:" ${S}/tlen.desktop
}

src_install() {
	# Prepare directory
	dodir ${TLEN_DIR}
	# Copy data
	cp -r data/* ${D}/${TLEN_DIR} || die "cp failed"
	# Install desktop file
	insinto /usr/share/applications/
	doins tlen.desktop
	# Install mime type
	insinto /usr/share/mime/packages/
	doins tlen.pl-extension.xml
	# Install symlink
	dodir /usr/bin
	dosym ${TLEN_DIR}/tlen /usr/bin/tlen
}

pkg_preinst() {
	check_license ${S}/data/license.txt
}

pkg_postinst() {
	fdo-mime_desktop_database_update
	fdo-mime_mime_database_update
	### @todo deactivate auto-update by default
	elog "It is not recommended to use Updater plugin."
	elog "You should deselect Updater in:"
	elog "\"Tlen\" -> \"Preferences\" -> \"Plugins\"."
	elog
}

pkg_postrm() {
	fdo-mime_desktop_database_update
	fdo-mime_mime_database_update
}
