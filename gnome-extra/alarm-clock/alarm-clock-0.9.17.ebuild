# Copyright 1999-2008 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

inherit distutils

DESCRIPTION="Alarm Clock is a personal alarm clock applet for the Gnome panel."
HOMEPAGE="http://alarm-clock.54.pl"
SRC_URI="http://89.76.224.23/downloads/${P}.tar.bz2"

LICENSE="GPL-2"
SLOT="0"
KEYWORDS="~amd64 ~ppc ~x86"

DEPEND=""
RDEPEND=">=dev-python/pygtk-2
	dev-python/notify-python
	dev-python/gst-python"

src_unpack() {
	distutils_src_unpack
	sed -i \
		-e "s:msgfmt.pyc:msgfmt.py:" \
		setup.py || die "sed failed"
}

src_compile() {
	distutils_src_compile
}

src_install() {
	distutils_src_install
}
