# Copyright 1999-2010 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI="2"

inherit eutils gnome2 python

MY_PN="awn-extras"
MY_P="${MY_PN}-${PV}"
PVS="0.4"

DESCRIPTION="Applets for Avant Window Navigator"
HOMEPAGE="https://launchpad.net/awn-extras"
SRC_URI="https://launchpad.net/awn-extras/${PVS}/${PV/_rc1}/+download/${MY_P/_/~}.tar.gz"
LICENSE="GPL-2"

SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE="gnome nls webkit xulrunner"

COMMON_DEPEND="
	>=dev-libs/dbus-glib-0.70
	>=dev-python/pygobject-2
	>=dev-python/pygtk-2
	>=gnome-extra/avant-window-navigator-0.4.0_rc1
	x11-libs/libnotify
	x11-libs/libsexy
	x11-libs/libXcomposite
	x11-libs/libXrender
	>=x11-libs/libwnck-2.22
	x11-libs/vte
	gnome? (
		gnome-base/gnome-menus
		>=gnome-base/gconf-2
		>=gnome-base/gnome-vfs-2
		>=gnome-base/libgtop-2
		gnome-base/librsvg
	)
	webkit? ( net-libs/webkit-gtk )
	xulrunner? ( net-libs/xulrunner )"
DEPEND="${COMMON_DEPEND}
	>=dev-util/intltool-0.35
	>=dev-util/pkgconfig-0.17"
RDEPEND="${COMMON_DEPEND}
	dev-python/feedparser
	dev-python/notify-python
	dev-python/pyalsaaudio
	gnome? (
		dev-python/gconf-python
		dev-python/gnome-applets-python
		dev-python/gnome-desktop-python
		dev-python/gnome-keyring-python
		dev-python/gnome-media-python
		dev-python/gnome-vfs-python
		dev-python/gtkmozembed-python
		dev-python/libgnomecanvas-python
		dev-python/libgnomeprint-python
		dev-python/libgnome-python
		dev-python/librsvg-python
		dev-python/libwnck-python
		gnome-base/gnome-menus[python]
	)"

DOCS="ChangeLog README"

S="${WORKDIR}/${MY_P/_/~}"

pkg_setup() {
	G2CONF="${G2CONF}
		--disable-pymod-checks
		--disable-shave
		--disable-static
		--with-gconf-schema-file-dir=/etc/gconf/schemas
		$(use_enable nls)
		$(use_with gnome gconf)
		$(use_with gnome)
		$(use_with webkit)
		$(use_with xulrunner mozilla)"
}

pkg_postinst() {
	gnome2_pkg_postinst
	python_mod_optimize $(python_get_sitedir)/awn/extras
}

pkg_postrm() {
	gnome2_pkg_postrm
	python_mod_cleanup awn/extras
}
