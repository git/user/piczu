# Copyright 1999-2008 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

inherit eutils distutils

DESCRIPTION="A Python written Linux Google Reader Notifier"
HOMEPAGE="http://grnotify.sourceforge.net"
SRC_URI="mirror://sourceforge/${PN}/${P}.tar.gz"

LICENSE="GPL-2"
SLOT="0"
KEYWORDS="~x86"

DEPEND=">=dev-lang/python-2.2
	dev-python/egg-python"
RDEPEND=">=dev-python/pygtk-2
	>=dev-python/pyxml-0.8.3
	dev-python/notify-python"

src_unpack() {
	distutils_src_unpack
	sed -i \
		-e "s:cp grnotify:mkdir /usr/bin/; sudo cp grnotify:" \
		-e "s:cp share/grnotify.desktop:mkdir /usr/share/applications; sudo cp share/grnotify.desktop:" \
		-e "s:/usr:' + sys.argv[1] + 'usr:g" \
		-e "s:mkdir:mkdir -p:" \
		-e "s:777:755:" \
		-e "35,40s:applications:applications/:" \
		install.py || die "sed failed"
}

src_compile() {
	return
}

src_install() {
	${python} install.py "${D}" ||\
			die "python install.py failed"
	fperms 644 /usr/share/applications/grnotify.desktop /usr/share/pixmaps/grnotify.xpm 
	fperms 644 /usr/share/grnotify/{about,config,feed}.glade /usr/share/pixmaps/grnotify/{broken,new,nonew}.gif
}
