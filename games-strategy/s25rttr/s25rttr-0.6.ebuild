# Copyright 1999-2010 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI=3

inherit games

if [ ${PV} = "9999" ]; then
	EBZR_REPO_URI="lp:s25rttr"
	inherit bzr
	SRC_URI=""
fi

DESCRIPTION="Opensource remake of The Settlers II game"
HOMEPAGE="http://www.siedler25.org/"

if [ ! ${PV} = "9999" ]; then
	S_U="http://launchpad.net/s25rttr/s25client/0.6/+download/${P/-/_}"
	SRC_URI="x86? ( ${S_U}_x86.tar.bz2 )
		amd64? ( ${S_U}_amd64.tar.bz2 )"
fi

LICENSE="GPL-2"
SLOT="0"
KEYWORDS="~x86 ~amd64"
IUSE=""

RDEPEND="media-libs/libsndfile
	media-libs/libsamplerate
	=media-libs/sdl-mixer-1.2*
	sys-devel/gettext"
#	media-libs/libtimidity"
DEPEND="${RDEPEND}"

S="${WORKDIR}/${P/-/_}"

src_install() {
	into /opt/${PN}
	insinto /opt/${PN}
	dobin bin/s25client
	doins -r lib share
	dodoc *.txt

	cat <<- EOF > s25client
		#!/usr/bin/env bash
		cd /opt/s25rttr
		bin/s25client

	EOF

	dogamesbin s25client
}

pkg_postinst() {
	games_pkg_postinst
	elog "Copy or symlink your original game files to /opt/s25rttr/share/s25rttr/S2/"
}
