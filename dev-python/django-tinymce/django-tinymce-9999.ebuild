# Copyright 1999-2008 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: Exp $

ESVN_REPO_URI="http://django-tinymce.googlecode.com/svn/trunk/"

inherit distutils eutils subversion

DESCRIPTION="TinyMCE widget for Django"
HOMEPAGE="http://code.google.com/p/django-tinymce/"
LICENSE="MIT"
SLOT="0"
KEYWORDS="~x86"
IUSE=""

RDEPEND="dev-python/django"
DEPEND="${RDEPEND}"

DOCS="docs/* README.txt"

src_install() {
	distutils_python_version

	site_pkgs="$(python_get_sitedir)"
	export PYTHONPATH="${PYTHONPATH}:${D}/${site_pkgs}"
	dodir ${site_pkgs}

	distutils_src_install
}
