# Copyright 1999-2008 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: Exp $
EAPI="2"

inherit distutils eutils

DESCRIPTION="TinyMCE widget for Django"
HOMEPAGE="http://code.google.com/p/django-tinymce/"
SRC_URI="http://django-tinymce.googlecode.com/files/${P}.tar.gz"

LICENSE="MIT"
SLOT="0"
KEYWORDS="~x86 ~amd64"
IUSE=""

RDEPEND="dev-python/django"
DEPEND="${RDEPEND}"

src_install() {
	distutils_src_install
	dodoc docs/* README.txt
}
