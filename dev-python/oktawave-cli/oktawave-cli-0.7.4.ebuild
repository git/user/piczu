# Copyright 1999-2013 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI=4

inherit eutils distutils

MY_P="${P}-src"

DESCRIPTION="Command line interface to Oktawave"
HOMEPAGE="http://www.oktawave.com"
SRC_URI="mirror://sourceforge/oktawave-cli/${MY_P}.tar.gz"

LICENSE="GPL-3"
SLOT="0"
KEYWORDS="~mips ~sparc ~x86 ~x86-macos ~x64-macos ~amd64 ~x86-linux ~amd64-linux"

RDEPEND="dev-python/suds
	dev-python/python-swiftclient
	dev-python/setproctitle
	dev-python/prettytable"
#	blocked by =python-2.7
#	dev-python/argparse
DEPEND="${RDEPEND}"

DOCS="README CHANGELOG"

src_prepare() {
    epatch "${FILESDIR}"/${PN}-no-data-files.patch
}
