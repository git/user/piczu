# Copyright 1999-2008 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: Exp $

EGIT_REPO_URI="git://github.com/robhudson/django-debug-toolbar.git"

inherit distutils eutils git

DESCRIPTION="A debugging toolbar for Django with various panels of information"
HOMEPAGE="http://code.google.com/p/django-debug-toolbar/"

LICENSE="BSD"
SLOT="0"
KEYWORDS="~amd64 ~ia64 ~ppc ~ppc64 ~x86"
IUSE=""

RDEPEND="dev-python/django"
DEPEND="${RDEPEND}"

DOCS="AUTHORS README.rst"

src_compile() {
	return
}

src_install() {
	distutils_python_version

	site_pkgs="$(python_get_sitedir)"
	export PYTHONPATH="${PYTHONPATH}:${D}/${site_pkgs}"
	insinto ${site_pkgs}
	doins -r debug_toolbar

	dodoc ${DOCS}
}

pkg_postinst() {
	elog
	elog "Middleware class placed in:"
	elog "  'debug_toolbar.middleware.DebugToolbarMiddleware'"
	elog "Toolbar panels palced in:"
	elog "  'debug_toolbar.panels.*'"
	elog


}
