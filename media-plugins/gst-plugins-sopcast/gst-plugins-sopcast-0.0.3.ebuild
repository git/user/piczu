# Copyright 1999-2009 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

inherit eutils

DESCRIPION="plugin to allow play sopcast stream"
HOMEPAGE="http://code.google.com/p/totem-sopcast"
SRC_URI="http://totem-sopcast.googlecode.com/files/${P}.tar.gz"

LICENSE="GPL-2"
SLOT="0"
KEYWORDS="~x86"
IUSE=""

RDEPEND="media-tv/sopcast
	>=media-libs/gstreamer-0.10.24
	>=media-libs/gst-plugins-base-0.10.24"
DEPEND="${RDEPEND}
	dev-util/pkgconfig"

src_install() {
    emake DESTDIR="${D}" install || die "Install failed"
    dodoc README ChangeLog || die
}
