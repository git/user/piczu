# Copyright 1999-2012 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI=3

inherit git-2

DESCRIPTION="Mint-X GTK themes"
HOMEPAGE="https://github.com/linuxmnt/mint-themes"
EGIT_REPO_URI="git://github.com/linuxmint/mint-themes.git"

LICENSE="GPL"
SLOT="0"
KEYWORDS="~amd64"
IUSE=""

DEPEND=""
RDEPEND="${DEPEND}"

src_install() {
	doins -r usr
}
