# Copyright 1999-2010 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $
EAPI=2

inherit nsplugins

DESCRIPTION="Adobe Flash Player"
HOMEPAGE="http://www.adobe.com/"
SRC_URI="http://download.macromedia.com/pub/labs/flashplayer10/flashplayer10_1_p3_linux_022310.tar.gz"

LICENSE="AdobeFlash-10"
SLOT="0"
KEYWORDS="~x86 ~amd64"
IUSE=""
RESTRICT="strip"

DEPEND="${RDEPEND}"
RDEPEND=">=sys-libs/glibc-2.3"

S="${WORKDIR}/install_flash_player_10_linux"

src_install() {
	exeinto /opt/netscape/plugins
	doexe libflashplayer.so
	inst_plugin /opt/netscape/plugins/libflashplayer.so
}
